include .env

init: docker-down docker-pull docker-build docker-up backend-init frontend-init
backend-init: composer-install refresh
frontend-init: npm-install npm-run-dev
migrate: wait-db migrations
seed: seed-test-users
refresh: wait-db refresh-migrations seed

### Docker ###
docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build


### Backend ###
create-project:
	docker-compose run --rm php-cli composer create-project laravel/laravel backend

composer-install:
	docker-compose run --rm php-cli composer install

wait-db:
	until docker-compose exec -T postgres pg_isready --timeout=0 --dbname=app ; do sleep 1; done

migrations:
	docker-compose run --rm php-cli php artisan migrate

refresh-migrations:
	docker-compose run --rm php-cli php artisan migrate:refresh

test:
	docker-compose run --rm php-cli php vendor/bin/phpunit

### Seed ###
seed-test-users:
	docker-compose run --rm php-cli php artisan db:seed --class=UserSeeder

### Frontend ###
npm-install:
	docker-compose run --rm node-cli npm install

npm-run-dev:
	docker-compose run --rm node-cli npm run dev


### Deploy ###
build-production:
	docker build --pull --file=docker/production/nginx/Dockerfile --tag ${REGISTRY_ADDRESS}/${APP_NAME}-nginx:${IMAGE_TAG} ./
	docker build --pull --file=docker/production/php-fpm/Dockerfile --tag ${REGISTRY_ADDRESS}/${APP_NAME}-php-fpm:${IMAGE_TAG} ./
	docker build --pull --file=docker/production/php-cli/Dockerfile --tag ${REGISTRY_ADDRESS}/${APP_NAME}-php-cli:${IMAGE_TAG} ./

push-production:
	docker push ${REGISTRY_ADDRESS}/${APP_NAME}-nginx:${IMAGE_TAG}
	docker push ${REGISTRY_ADDRESS}/${APP_NAME}-php-fpm:${IMAGE_TAG}
	docker push ${REGISTRY_ADDRESS}/${APP_NAME}-php-cli:${IMAGE_TAG}

deploy:
	ssh deploy@${HOST} -p ${PORT} 'rm -rf ${APP_NAME}_${BUILD_NUMBER}'
	ssh deploy@${HOST} -p ${PORT} 'mkdir -p ${APP_NAME}_${BUILD_NUMBER}'
	scp -P ${PORT} docker-compose-production.yml deploy@${HOST}:${APP_NAME}_${BUILD_NUMBER}/docker-compose.yml
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "APP_NAME=${APP_NAME}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "COMPOSE_PROJECT_NAME=${APP_NAME}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "REGISTRY_ADDRESS=${REGISTRY_ADDRESS}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "IMAGE_TAG=${IMAGE_TAG}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "DB_PASSWORD=${DB_PASSWORD}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "DB_DATABASE=${DB_DATABASE}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "DB_USERNAME=${DB_USERNAME}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "INSTAGRAM_USERNAME=${INSTAGRAM_USERNAME}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && echo "INSTAGRAM_PASSWORD=${INSTAGRAM_PASSWORD}" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && docker-compose pull'
	ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && docker-compose up --build --remove-orphans -d'
	#ssh deploy@${HOST} -p ${PORT} 'cd ${APP_NAME}_${BUILD_NUMBER} && docker-compose run php-cli php artisan migrate --force'
	ssh deploy@${HOST} -p ${PORT} 'rm -f ${APP_NAME}'
	ssh deploy@${HOST} -p ${PORT} 'ln -sr ${APP_NAME}_${BUILD_NUMBER} ${APP_NAME}'