@component('mail::message')

Здравствуйте! Вы успешно зарегистрированы на <a href="{{ route('home') }}">tsenoskop.ru</a>.
Для продолжения подтвердите Ваш email адрес.

@component('mail::button', ['url' => route('confirm-signup-page', ['token' => $token])])
Подтвердить
@endcomponent

Спасибо,<br>
{{ config('app.name') }}
@endcomponent
