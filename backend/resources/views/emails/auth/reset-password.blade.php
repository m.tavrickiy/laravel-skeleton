@component('mail::message')

Пожалуйста, перейдите по ссылке для восстановления пароля.

@component('mail::button', ['url' => route('reset-password-page', ['token' => $token])])
Сбросить пароль
@endcomponent

Спасибо,<br>
{{ config('app.name') }}
@endcomponent
