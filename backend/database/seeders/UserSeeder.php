<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User\Entity\User\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->admin()->email('admin@admin.ru')->create();
    }
}
