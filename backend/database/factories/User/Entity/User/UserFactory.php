<?php

namespace Database\Factories\User\Entity\User;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\User\Entity\Role\AdminRole;
use App\Models\User\Entity\User\User;
use App\Models\User\Entity\User\Email;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->uuid,
            'name' => $this->faker->name,
            'email' => new Email($this->faker->unique()->safeEmail),
            'role' => new AdminRole(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function admin()
    {
        return $this->state(function (array $attributes) {
            return [
                'role' => new AdminRole,
            ];
        });
    }

    /**
     * @param $email
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function email($email)
    {
        return $this->state(function (array $attributes) use ($email) {
            return [
                'email' => new Email($email),
            ];
        });
    }
}
