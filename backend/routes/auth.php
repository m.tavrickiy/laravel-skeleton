<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use Illuminate\Support\Facades\Route;


Route::get('/user/signup', [\App\Http\Controllers\Auth\SignUpController::class, 'page'])->middleware('guest');
Route::post('/user/signup', [\App\Http\Controllers\Auth\SignUpController::class, 'signUp'])->middleware('guest')->name('signup');

Route::get('/user/confirm-signup', [\App\Http\Controllers\Auth\ConfirmSignUpController::class, 'page'])->middleware('guest')->name('confirm-signup-page');
Route::post('/user/confirm-signup', [\App\Http\Controllers\Auth\ConfirmSignUpController::class, 'confirmSignUp'])->middleware('guest')->name('confirm-signup');

Route::get('/user/reset-token', [\App\Http\Controllers\Auth\GenerateResetTokenController::class, 'page'])->middleware('guest')->name('reset-token-page');
Route::post('/user/reset-token', [\App\Http\Controllers\Auth\GenerateResetTokenController::class, 'token'])->middleware('guest')->name('reset-token');
Route::get('/user/reset-password', [\App\Http\Controllers\Auth\ResetPasswordController::class, 'page'])->middleware('guest')->name('reset-password-page');
Route::post('/user/reset-password', [\App\Http\Controllers\Auth\ResetPasswordController::class, 'resetPassword'])->middleware('guest')->name('reset-password');


Route::get('/login', [AuthenticatedSessionController::class, 'create'])
    ->middleware('guest')
    ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
    ->middleware('guest');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
    ->middleware('auth')
    ->name('logout');
