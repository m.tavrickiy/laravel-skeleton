<?php

namespace App\Mail\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User\Entity\User\Email;
use App\Models\User\Entity\User\ResetToken;

class ResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    public Email  $email;
    public string $token;

    public function __construct(Email $email, ResetToken $token)
    {
        $this->email = $email;
        $this->token = $token->getToken();
    }

    public function build()
    {
        return $this->to($this->email->getValue())->markdown('emails.auth.reset-password');
    }
}
