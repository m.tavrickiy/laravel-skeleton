<?php

namespace App\Models\User\Entity\User;

use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class ResetToken
{
    private string $token;
    private \DateTimeImmutable $expired;

    public function __construct(string $token, \DateTimeImmutable $expired)
    {
        Assert::notEmpty($token);
        $this->token   = $token;
        $this->expired = $expired;
    }

    public function check(string $token): bool
    {
        return $this->token === $token;
    }

    public function isExpired(\DateTimeImmutable $date): bool
    {
        return $date > $this->expired;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getExpired(): \DateTimeImmutable
    {
        return $this->expired;
    }
}
