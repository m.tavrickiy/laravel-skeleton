<?php

namespace App\Models\User\Entity\User;

use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_socials")
 */
class Social
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @var string
     */
    private string $id;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="socials", inversedBy="socials")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @var User
     */
    private User   $user;
    private string $social;
    private string $identity;

    public function __construct(User $user, string $social, string $identity)
    {
        $this->id       = Uuid::uuid4()->toString();
        $this->user     = $user;
        $this->social   = $social;
        $this->identity = $identity;
    }

    public function isSocialFor(string $social): bool
    {
        return $social === $this->social;
    }

    public function getSocial(): string
    {
        return $this->social;
    }

    public function getIdentity(): string
    {
        return $this->identity;
    }
}
