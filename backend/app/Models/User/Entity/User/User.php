<?php

namespace App\Models\User\Entity\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Id;
use App\Models\User\Entity\User\Cast\IdCast;
use App\Models\User\Entity\User\Cast\EmailCast;
use App\Models\User\Exception\User\UserHasAlreadyConfirmedException;
use App\Models\User\Exception\User\ResetPassword\ResetTokenWasAlreadyGeneratedException;
use App\Models\User\Entity\User\Cast\ResetTokenCast;
use App\Models\User\Exception\User\ResetPassword\InvalidResetTokenException;
use App\Models\User\Entity\User\Cast\BalanceCast;
use App\Models\User\Entity\Role\Role;
use App\Models\User\Entity\Role\Cast\RoleCast;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property Id $id
 * @property string $name
 * @property Role $role
 * @property Email $email
 * @property \DateTime|null $email_verified_at
 * @property string|null $confirm_token
 * @property ResetToken|null $resetToken
 * @property \DateTime|null $created_at
 * @property Balance $balance
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => IdCast::class,
        'email'             => EmailCast::class,
        'resetToken'        => ResetTokenCast::class,
        'email_verified_at' => 'datetime',
        'balance'           => BalanceCast::class,
        'role'              => RoleCast::class,
    ];

    public static function new(Id $id, string $name, Email $email, Role $role, \DateTimeImmutable $createdAt, string $token): self
    {
        $user                = new self;
        $user->id            = $id;
        $user->email         = $email;
        $user->name          = $name;
        $user->created_at    = $createdAt;
        $user->confirm_token = $token;
        $user->balance       = new Balance(0);
        $user->role          = $role;

        return $user;
    }

    public function confirmSignUp(string $password): void
    {
        if (!is_null($this->email_verified_at)) {
            throw new UserHasAlreadyConfirmedException('Почта пользователя уже была подтверждена.');
        }

        $this->email_verified_at = new \DateTimeImmutable;
        $this->confirm_token     = null;
        $this->password          = $password;
    }

    public function setResetToken(ResetToken $token): void
    {
        if (!is_null($this->resetToken) && !$this->resetToken->isExpired(new \DateTimeImmutable)) {
            throw new ResetTokenWasAlreadyGeneratedException('Токен для сброса пароля уже был запрошен.');
        }

        $this->resetToken = $token;
    }

    public function resetPassword(string $resetToken, string $password): void
    {
        if (!$this->resetToken->check($resetToken) || is_null($this->resetToken) || $this->resetToken->isExpired(new \DateTimeImmutable)) {
            throw new InvalidResetTokenException('Некорректный или уже использованный токен.');
        }

        $this->resetToken = null;
        $this->password   = $password;
    }

    public function addBalance(int $amount): void
    {
        $this->balance->add($amount);
    }

    public function subBalance(int $amount): void
    {
        $this->balance->sub($amount);
    }
}
