<?php

namespace App\Models\User\Entity\User;

use App\Models\Id;

class UserRepository
{
    /**
     * @param Id $id
     * @return User|object
     * @throws EntityNotFoundException
     */
    public function get(Id $id): User
    {
        if (!$user = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('User not found.');
        }

        return $user;
    }

    public function add(User $user): void
    {
        $user->saveOrFail();
    }

    public function update(User $user): void
    {
        $user->saveOrFail();
    }

    /**
     * @param string $token
     * @return User|null|object
     */
    public function findByConfirmToken(string $token): ?User
    {
        return User::query()->where('confirm_token', $token)->first();
    }

    /**
     * @param string $token
     * @return User|null|object
     */
    public function findByResetToken(string $token): ?User
    {
        return User::query()->where('reset_token', $token)->first();
    }

    /**
     * @param Email $email
     * @return User|null|object
     */
    public function findByEmail(Email $email): ?User
    {
        return User::query()->where('email', $email->getValue())->first();
    }
}
