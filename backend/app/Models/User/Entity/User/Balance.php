<?php

namespace App\Models\User\Entity\User;

class Balance
{
    public int $balance;

    public function __construct(int $balance)
    {
        $this->balance = $balance;
    }

    public function getBalance(): int
    {
        return $this->balance;
    }

    public function add(int $amount): void
    {
        $this->balance += $amount;
    }

    public function sub(int $amount): void
    {
        $this->balance -= $amount;
    }
}
