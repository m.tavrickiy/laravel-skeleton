<?php

namespace App\Models\User\Entity\User\Cast;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use App\Models\User\Entity\User\Email;

class EmailCast implements CastsAttributes
{
    /**
     * @inheritDoc
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return new Email($value);
    }

    /**
     * @inheritDoc
     */
    public function set($model, string $key, $value, array $attributes)
    {
        if (!$value instanceof Email) {
            throw new \InvalidArgumentException('Значение должно быть типа Email.');
        }

        return $value->getValue();
    }
}
