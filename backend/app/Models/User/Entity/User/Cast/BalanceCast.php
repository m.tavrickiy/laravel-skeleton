<?php

namespace App\Models\User\Entity\User\Cast;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use App\Models\User\Entity\User\Balance;

class BalanceCast implements CastsAttributes
{
    /**
     * @inheritDoc
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return new Balance($value);
    }

    /**
     * @inheritDoc
     */
    public function set($model, string $key, $value, array $attributes)
    {
        if (!$value instanceof Balance) {
            throw new \InvalidArgumentException('Значение должно быть типа Balance.');
        }

        return $value->getBalance();
    }
}
