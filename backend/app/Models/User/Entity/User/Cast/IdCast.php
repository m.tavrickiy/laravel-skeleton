<?php

namespace App\Models\User\Entity\User\Cast;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use App\Models\Id;

class IdCast implements CastsAttributes
{
    /**
     * @inheritDoc
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return new Id($value);
    }

    /**
     * @inheritDoc
     */
    public function set($model, string $key, $value, array $attributes)
    {
        return $value instanceof Id ? $value->getValue() : $value;
    }
}
