<?php

namespace App\Models\User\Entity\User\Cast;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use App\Models\User\Entity\User\ResetToken;

class ResetTokenCast implements CastsAttributes
{
    /**
     * @inheritDoc
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return $attributes['reset_token'] && $attributes['reset_token_expired_at'] ?
            new ResetToken(
                $model->reset_token,
                new \DateTimeImmutable($model->reset_token_expired_at)
            ) : null;
    }

    /**
     * @inheritDoc
     */
    public function set($model, string $key, $value, array $attributes)
    {
        if (!$value instanceof ResetToken && !is_null($value)) {
            throw new \InvalidArgumentException('Значение должно быть типа ResetToken.');
        }

        return [
            'reset_token'            => $value ? $value->getToken() : null,
            'reset_token_expired_at' => $value ? $value->getExpired() : null,
        ];
    }
}
