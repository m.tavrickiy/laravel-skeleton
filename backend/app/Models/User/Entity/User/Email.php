<?php

namespace App\Models\User\Entity\User;

use Webmozart\Assert\Assert;

class Email
{
    public string $value;

    public function __construct(string $email)
    {
        Assert::notEmpty($email);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException('Incorrect email.');
        }
        $this->value = mb_strtolower($email);
    }

    /**
     * @return false|string|string[]|null
     */
    public function getValue()
    {
        return $this->value;
    }
}
