<?php

namespace App\Models\User\Entity\Role;

class AdminRole extends Role
{
    public const VALUE = 'admin';

    public function getValue(): string
    {
        return self::VALUE;
    }
}
