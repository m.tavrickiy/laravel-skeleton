<?php

namespace App\Models\User\Entity\Role;

class RoleFactory
{
    public static function create(string $role)
    {
        switch ($role) {
            case AdminRole::VALUE:
                return new AdminRole;
            default:
                throw new \Exception('Unexpected value');
        }
    }
}
