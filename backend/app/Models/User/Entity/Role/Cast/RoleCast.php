<?php

namespace App\Models\User\Entity\Role\Cast;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use App\Models\User\Entity\Role\Role;
use App\Models\User\Entity\Role\RoleFactory;

class RoleCast implements CastsAttributes
{
    /**
     * @inheritDoc
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return RoleFactory::create($value);
    }

    /**
     * @inheritDoc
     */
    public function set($model, string $key, $value, array $attributes)
    {
        if (!$value instanceof Role) {
            throw new \InvalidArgumentException('Значение должно быть типа Role.');
        }

        return $value->getValue();
    }
}
