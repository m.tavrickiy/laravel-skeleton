<?php

namespace App\Models\User\Entity\Role;

abstract class Role
{
    abstract public function getValue(): string;

    public function equal(self $status): bool
    {
        return $this->getValue() === $status->getValue();
    }
}
