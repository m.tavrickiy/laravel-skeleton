<?php

namespace App\Models\User\Service;

use App\Models\User\Entity\User\Email;
use Illuminate\Contracts\Mail\Mailer;
use App\Mail\Auth\ResetPasswordEmail;
use App\Models\User\Entity\User\ResetToken;

class ResetTokenSender
{
    private Mailer $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(Email $email, ResetToken $token): void
    {
        $message = new ResetPasswordEmail($email, $token);
        $this->mailer->send($message);
    }
}
