<?php

namespace App\Models\User\Service;

use Ramsey\Uuid\Uuid;
use App\Models\User\Entity\User\ResetToken;

class ResetPasswordTokenizer
{
    /**
     * @var \DateInterval
     */
    private \DateInterval $interval;

    public function __construct(\DateInterval $interval)
    {
        $this->interval = $interval;
    }

    public function generate(): ResetToken
    {
        return new ResetToken(
            Uuid::uuid4()->toString(),
            (new \DateTimeImmutable)->add($this->interval)
        );
    }
}
