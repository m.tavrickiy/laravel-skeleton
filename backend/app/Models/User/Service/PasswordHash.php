<?php

namespace App\Models\User\Service;

use Illuminate\Support\Facades\Hash;

class PasswordHash
{
    public function generate(string $password): string
    {
        return Hash::make($password);
    }
}
