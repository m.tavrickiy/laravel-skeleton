<?php

namespace App\Models\User\Service;

use App\Models\User\Entity\User\Email;
use Illuminate\Contracts\Mail\Mailer;
use App\Mail\Auth\ConfirmEmail;

class ConfirmTokenSender
{
    private Mailer $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(Email $email, string $token): void
    {
        $message = new ConfirmEmail($email, $token);
        $this->mailer->send($message);
    }
}
