<?php

namespace App\Models\User\UseCase\SignUp;

class Command
{
    public string $email;
    public string $name;
    public string $role;

    public function __construct(string $name, string $email, string $role)
    {
        $this->email = $email;
        $this->name  = $name;
        $this->role  = $role;
    }
}
