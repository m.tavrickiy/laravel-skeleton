<?php

namespace App\Models\User\UseCase\SignUp;

use App\Models\User\Entity\User\UserRepository;
use App\Models\User\Entity\User\Email;
use App\Models\User\Entity\User\User;
use App\Models\User\Service\Tokenizer;
use App\Models\User\Service\ConfirmTokenSender;
use App\Models\Id;
use App\Models\User\Entity\Role\RoleFactory;

class Handler
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;
    /**
     * @var Tokenizer
     */
    private Tokenizer $tokenizer;
    /**
     * @var ConfirmTokenSender
     */
    private ConfirmTokenSender $sender;

    public function __construct(
        UserRepository $userRepository,
        Tokenizer $tokenizer,
        ConfirmTokenSender $sender
    )
    {
        $this->userRepository = $userRepository;
        $this->tokenizer      = $tokenizer;
        $this->sender         = $sender;
    }

    public function handle(Command $command): void
    {
        $email = new Email($command->email);

        if ($this->userRepository->findByEmail($email)) {
            throw new \DomainException('Пользователь с данным email уже существует.');
        }

        $user = User::new(
            Id::next(),
            $command->name,
            $email,
            RoleFactory::create($command->role),
            new \DateTimeImmutable(),
            $token = $this->tokenizer->generate()
        );

        $this->userRepository->add($user);
        $this->sender->send($email, $token);
    }
}
