<?php

namespace App\Models\User\UseCase\GenerateResetToken;

class Command
{
    public string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }
}
