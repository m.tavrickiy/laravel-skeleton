<?php

namespace App\Models\User\UseCase\GenerateResetToken;

use App\Models\User\Entity\User\UserRepository;
use App\Models\User\Entity\User\Email;
use App\Models\User\Service\Tokenizer;
use App\Models\User\Entity\User\ResetToken;
use App\Models\User\Service\ResetTokenSender;

class Handler
{
    private UserRepository $userRepository;
    private Tokenizer $tokenizer;
    /**
     * @var ResetTokenSender
     */
    private ResetTokenSender $resetTokenSender;

    public function __construct(
        UserRepository $userRepository,
        Tokenizer $tokenizer,
        ResetTokenSender $resetTokenSender
    )
    {
        $this->userRepository   = $userRepository;
        $this->tokenizer        = $tokenizer;
        $this->resetTokenSender = $resetTokenSender;
    }

    public function handle(Command $command): void
    {
        $email = new Email($command->email);

        if (!$user = $this->userRepository->findByEmail($email)) {
            throw new \DomainException('Пользователь не найден.');
        }

        $resetToken = new ResetToken(
            $this->tokenizer->generate(),
            (new \DateTimeImmutable)->add(new \DateInterval('P1D')),
        );
        $user->setResetToken($resetToken);
        $this->userRepository->update($user);
        $this->resetTokenSender->send($email, $resetToken);
    }
}
