<?php

namespace App\Models\User\UseCase\ResetPassword;

use App\Models\User\Entity\User\UserRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\User\Service\PasswordHash;

class Handler
{
    private UserRepository $userRepository;
    /**
     * @var PasswordHash
     */
    private PasswordHash $passwordHash;

    public function __construct(
        UserRepository $userRepository,
        PasswordHash $passwordHash
    )
    {
        $this->userRepository = $userRepository;
        $this->passwordHash   = $passwordHash;
    }

    public function handle(Command $command): void
    {
        if (!$user = $this->userRepository->findByResetToken($command->token)) {
            throw new \DomainException('Некорректный или уже использованный токен.');
        }

        $hashedPassword = $this->passwordHash->generate($command->password);
        $user->resetPassword($command->token, $hashedPassword);
        $this->userRepository->update($user);
        Auth::login($user);
    }
}
