<?php

namespace App\Models\User\Exception\User\Social;

class SocialHasAlreadyAttachedException extends \DomainException
{
}
