<?php

namespace App\Models\User\Exception\User\ResetPassword;

class TokenIsAbsentException extends \DomainException
{
}
