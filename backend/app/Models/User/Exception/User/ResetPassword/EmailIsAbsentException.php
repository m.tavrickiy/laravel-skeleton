<?php

namespace App\Models\User\Exception\User\ResetPassword;

class EmailIsAbsentException extends \DomainException
{
}
