<?php

namespace App\Models\User\Exception\User\ResetPassword;

class ResetTokenWasExpiredException extends \DomainException
{
}
