<?php

namespace App\Models\User\Exception\User\ResetPassword;

class ResetTokenWasAlreadyGeneratedException extends \DomainException
{
}
