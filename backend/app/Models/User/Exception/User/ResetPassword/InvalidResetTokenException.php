<?php

namespace App\Models\User\Exception\User\ResetPassword;

class InvalidResetTokenException extends \DomainException
{
}
