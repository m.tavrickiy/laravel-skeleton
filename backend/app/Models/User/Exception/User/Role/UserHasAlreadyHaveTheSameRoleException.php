<?php

namespace App\Models\User\Exception\User\Role;

class UserHasAlreadyHaveTheSameRoleException extends \DomainException
{
}
