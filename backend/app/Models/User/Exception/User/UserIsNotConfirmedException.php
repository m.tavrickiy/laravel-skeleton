<?php

namespace App\Models\User\Exception\User;

class UserIsNotConfirmedException extends \DomainException
{
}
