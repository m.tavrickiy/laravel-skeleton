<?php

namespace App\Models\User\Exception\User;

class UserHasAlreadyConfirmedException extends \DomainException
{
}
