<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $token
 * @property string $password
 */
class Confirm extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'token'    => ['required', 'string'],
            'password' => ['required', 'confirmed'],
        ];
    }
}
