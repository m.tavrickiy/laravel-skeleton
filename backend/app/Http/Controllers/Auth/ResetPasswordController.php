<?php

namespace App\Http\Controllers\Auth;

use App\Models\User\UseCase\ResetPassword\Handler;
use App\Models\User\UseCase\ResetPassword\Command;
use Illuminate\Http\Request;
use App\Http\Requests\User\ResetPassword;

class ResetPasswordController
{
    public function page(Request $request)
    {
        return view('auth.reset-password', ['request' => $request]);
    }

    public function resetPassword(ResetPassword $request, Handler $handler)
    {
        $handler->handle(
            new Command($request->token, $request->password)
        );

        return redirect()->route('login');
    }
}
