<?php

namespace App\Http\Controllers\Auth;

use App\Models\User\UseCase\Confirm\Handler;
use App\Models\User\UseCase\Confirm\Command;
use App\Http\Requests\User\Confirm;
use Illuminate\Http\Request;

class ConfirmSignUpController
{
    public function page(Request $request)
    {
        return view('auth.confirm-signup', ['request' => $request]);
    }

    public function confirmSignUp(Confirm $request, Handler $handler)
    {
        $handler->handle(
            new Command($request->token, $request->password)
        );

        return redirect()->route('login');
    }
}
