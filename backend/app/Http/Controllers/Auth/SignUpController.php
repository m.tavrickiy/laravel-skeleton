<?php

namespace App\Http\Controllers\Auth;

use App\Models\User\UseCase\SignUp\Handler;
use App\Models\User\UseCase\SignUp\Command;
use App\Http\Requests\User\SignUp;

class SignUpController
{
    public function page()
    {
        return view('auth.signup');
    }

    public function signUp(SignUp $request, Handler $handler)
    {
        $handler->handle(
            new Command($request->name, $request->email)
        );

        return redirect()->route('login');
    }
}
