<?php

namespace App\Http\Controllers\Auth;

use App\Models\User\UseCase\GenerateResetToken\Handler;
use App\Models\User\UseCase\GenerateResetToken\Command;
use Illuminate\Http\Request;
use App\Http\Requests\User\GenerateResetToken;

class GenerateResetTokenController
{
    public function page(Request $request)
    {
        return view('auth.generate-reset-token', ['request' => $request]);
    }

    public function token(GenerateResetToken $request, Handler $handler)
    {
        try {
            $handler->handle(
                new Command($request->email)
            );
        } catch (\DomainException $exception) {
            return redirect()->back()->with(['error' => $exception->getMessage()]);
        }

        return redirect()->back()->with(['status' => "Письмо было отправлено на почту {$request->email}."]);
    }
}
